import os
import sys
import time
import django
import random


RUNS = 25

# documentation on django's queryset lazy access to db

# https://docs.djangoproject.com/en/dev/ref/models/querysets/#when-querysets-are-evaluated
# acessing callables make db run query
# transforming querysets to lists also forces query to run

# https://docs.djangoproject.com/en/3.0/topics/db/optimization/#retrieve-everything-at-once-if-you-know-you-will-need-it
# https://docs.djangoproject.com/en/3.0/ref/models/querysets/
# https://docs.djangoproject.com/en/3.0/topics/db/optimization/
# https://stackoverflow.com/questions/5989499/django-orm-and-hitting-db


def show_results(results, command, literal_command):
    best = [
        min(results[0]),
        min(results[1]),
        min(results[2]),
        min(results[3]),
    ]
    worst = [
        max(results[0]),
        max(results[1]),
        max(results[2]),
        max(results[3]),
    ]
    avrg = [
        sum(results[0]) / len(results[0]),
        sum(results[1]) / len(results[1]),
        sum(results[2]) / len(results[2]),
        sum(results[3]) / len(results[3]),
    ]
    print('-------------------------------------------------------------------')
    print('Results (ran "%s" for each model %d times):' % (command, RUNS))
    print('Cmd: %s' % literal_command)
    if command is 'get_all_query':
        print('\nSample:\n - average:   %fs' % best[0])
        print('\nVerticalSample:\n - average:   %fs' % best[1])
        print('\nAltVerticalSample:\n - average:   %fs' % best[2])

    else:
        print('\nSample:\n - results: %s\n - best:      %fs\n - worst:     %fs\n - average:   %fs' %
              (results[0], best[0], worst[0], avrg[0]))
        print('\nHybridSample:\n - results: %s\n - best:      %fs\n - worst:     %fs\n - average:   %fs' %
              (results[1], best[1], worst[1], avrg[1]))
        print('\nVerticalSample:\n - results: %s\n - best:      %fs\n - worst:     %fs\n - average:   %fs' %
              (results[2], best[2], worst[2], avrg[2]))
        print('\nAltVerticalSample:\n results: %s\n - best:      %fs\n - worst:     %fs\n - average:   %fs' %
              (results[3], best[3], worst[3], avrg[3]))


def get_all_query(klass):
    results = []
    init = time.time()
    for i in range(RUNS):
        bool(klass.objects.all())
    elapsed = (time.time() - init)
    results.append(elapsed / RUNS)
    return results


def get_filter_query(klass, device, vertical=False, alt=False):
    if vertical:
        if alt:
            #query = device.measureable_vertical_events.filter(samples=None)
            event = MeasureableVerticalEvent.objects.get(id=191801)
            # event = query[0]
            # event = query[random.randint(0, len(query) - 1)]

        else:
            query = device.measureable_vertical_events.filter(altvsamples=None)
            event = query[0]
            # event = query[random.randint(0, len(query) - 1)]
    else:
        query = device.measureable_events.filter(jsample=None)
        event = query[0]
        # event = query[random.randint(0, len(query) - 1)]

    results = []

    init = time.time()
    for i in range(RUNS):
        a = list(klass.objects.filter(event=event))
    elapsed = (time.time() - init)
    results.append(elapsed / RUNS)
    return results


def get_device_samples_through_events(device):
    results = []

    init = time.time()
    for i in range(RUNS):
        a = device.measureable_events.first().sample.id
    elapsed = (time.time() - init)
    results.append(elapsed / RUNS)

    init = time.time()
    for i in range(RUNS):
        a = device.measureable_events.last().sample.id
    elapsed = (time.time() - init)
    results.append(elapsed / RUNS)

    return results


def get_device_hsamples_through_events(device):
    results = []

    init = time.time()
    for i in range(RUNS):
        a = device.measureable_events.first().sample.id
    elapsed = (time.time() - init)
    results.append(elapsed / RUNS)

    init = time.time()
    for i in range(RUNS):
        a = list(device.measureable_events.last().hsamples.all())
    elapsed = (time.time() - init)
    results.append(elapsed / RUNS)

    return results


def get_device_v_samples_through_events(device):
    results = []

    init = time.time()
    for i in range(RUNS):
        a = list(device.measureable_vertical_events.first().samples.all())
    elapsed = (time.time() - init)
    results.append(elapsed / RUNS)

    init = time.time()
    for i in range(RUNS):
        a = bool(device.measureable_vertical_events.last().samples.all())
    elapsed = (time.time() - init)
    results.append(elapsed / RUNS)

    return results


def get_device_altv_samples_through_events(device):
    results = []

    init = time.time()
    for i in range(RUNS):
        a = list(device.measureable_vertical_events.first().altvsamples.all())
    elapsed = (time.time() - init)
    results.append(elapsed / RUNS)
    command = '%s.measureable_vertical_events.first().altvsamples.all()' % device

    init = time.time()
    for i in range(RUNS):
        a = bool(device.measureable_vertical_events.last().altvsamples.all())
    elapsed = (time.time() - init)
    results.append(elapsed / RUNS)
    command = '%s.measureable_vertical_events.last().altvsamples.all()' % device

    return results


if __name__ == "__main__":
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dbtest.settings')
    django.setup()

    from dbtest.events.models import *
    from dbtest.devices.models import *
    from dbtest.samples.models import *
    from dbtest.device_types.models import *

    print('Device -> %d objects' % Device.objects.count())
    print('Sample -> %d objects' % Sample.objects.count())
    print('HybridSample -> %d objects' % HybridSample.objects.count())
    print('VerticalSample -> %d objects' % VerticalSample.objects.count())
    print('AltVerticalSample -> %d objects\n' %
          AltVerticalSample.objects.count())

    devices = Device.objects.all()
    # first_device = Device.objects.first()
    # middle_device = Device.objects.get(id=10)   # running local
    # last_device = Device.objects.last()
    first_device = devices[0]
    middle_device = devices[50]
    last_device = devices[100]

    print('Using %d devices, since they have associated events' % 100)

    res = []
    res.append(get_all_query(Sample))
    res.append(get_all_query(HybridSample))
    res.append(get_all_query(VerticalSample))
    res.append(get_all_query(AltVerticalSample))
    show_results(
        res,
        'get_all_query',
        'Model.objects.all()'
    )

    # first_device = Device.objects.first()
    # middle_device = Device.objects.get(id=10)   # running local
    # last_device = Device.objects.last()
    first_device = devices[0]
    middle_device = devices[50]
    last_device = devices[100]

    res = []
    res.append(get_filter_query(Sample, first_device))
    res.append(get_filter_query(HybridSample, first_device))
    res.append(get_filter_query(VerticalSample, first_device, vertical=True))
    res.append(get_filter_query(AltVerticalSample,
                                first_device, vertical=True, alt=True))
    show_results(
        res,
        'get_filter_query_first_device',
        'Model.objects.filter(event=event)'
    )

    res = []
    res.append(get_filter_query(Sample, middle_device))
    res.append(get_filter_query(HybridSample, middle_device))
    res.append(get_filter_query(VerticalSample, middle_device, vertical=True))
    res.append(get_filter_query(AltVerticalSample,
                                middle_device, vertical=True, alt=True))
    show_results(
        res,
        'get_filter_query_middle_device',
        'Model.objects.filter(event=event)'
    )

    res = []
    res.append(get_filter_query(Sample, last_device))
    res.append(get_filter_query(HybridSample, last_device))
    res.append(get_filter_query(VerticalSample, last_device, vertical=True))
    res.append(get_filter_query(AltVerticalSample,
                                last_device, vertical=True, alt=True))
    show_results(
        res,
        'get_filter_query_last_device',
        'Model.objects.filter(event=event)'
    )

    res = []
    res.append(get_device_samples_through_events(first_device))
    res.append(get_device_hsamples_through_events(first_device))
    res.append(get_device_v_samples_through_events(first_device))
    res.append(get_device_altv_samples_through_events(first_device))
    show_results(
        res,
        'get_device_samples_through_events_first_device',
        'device.[measureable_events | measureable_vertical_events].[first | last]().[sample | v_samples.all()]'
    )

    res = []
    res.append(get_device_samples_through_events(middle_device))
    res.append(get_device_hsamples_through_events(middle_device))
    res.append(get_device_v_samples_through_events(middle_device))
    res.append(get_device_altv_samples_through_events(middle_device))
    show_results(
        res,
        'get_device_samples_through_events_middle_device',
        'device.[measureable_events | measureable_vertical_events].[first | last]().[sample | v_samples.all()]'
    )

    res = []
    res.append(get_device_samples_through_events(last_device))
    res.append(get_device_hsamples_through_events(last_device))
    res.append(get_device_v_samples_through_events(last_device))
    res.append(get_device_altv_samples_through_events(last_device))
    show_results(
        res,
        'get_device_samples_through_events_last_device',
        'device.[measureable_events | measureable_vertical_events].[first | last]().[sample | v_samples.all()]'
    )
