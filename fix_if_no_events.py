import os
import sys
import time
import django
import random
import threading


def random_float():
    rand = random.random()
    return random.uniform(rand, rand*15)


def pop_events(devices):
    for device in devices:
        # gen_vertical_events(device)
        print('poping events for device %d' % device.id)
        gen_events(device)
        print('Checking if any measureable events have no sample and fixing it...')
        events = MeasureableEvent.objects.filter(
            device=device, sample=None)
        pop_samples(events, device)
    print('thread is finished!')


def pop_samples(events, device):
    for index, e in enumerate(events):
        if index % 2 == 0:

            s1 = Sample.objects.create(
                event=e,
                device=device,
                end_of_life_1=True,
                end_of_life_2=True,
                end_of_life_3=True,
                end_of_life_4=True,
                has_tension_1=True,
                has_tension_2=True,
                has_tension_3=True,
                has_tension_4=True,
                surge_tension_phase_a_1=random_float(),
                surge_tension_phase_a_2=random_float(),
                surge_tension_phase_a_3=random_float(),
                surge_tension_phase_a_4=random_float(),
                surge_tension_phase_b_1=random_float(),
                surge_tension_phase_b_2=random_float(),
                surge_tension_phase_b_3=random_float(),
                surge_tension_phase_b_4=random_float(),
                surge_tension_phase_c_1=random_float(),
                surge_tension_phase_c_2=random_float(),
                surge_tension_phase_c_3=random_float(),
                surge_tension_phase_c_4=random_float(),
                surge_current_phase_a_1=random_float(),
                surge_current_phase_a_2=random_float(),
                surge_current_phase_a_3=random_float(),
                surge_current_phase_a_4=random_float(),
                surge_current_phase_b_1=random_float(),
                surge_current_phase_b_2=random_float(),
                surge_current_phase_b_3=random_float(),
                surge_current_phase_b_4=random_float(),
                surge_current_phase_c_1=random_float(),
                surge_current_phase_c_2=random_float(),
                surge_current_phase_c_3=random_float(),
                surge_current_phase_c_4=random_float(),
                escape_current_phase_a_1=random_float(),
                escape_current_phase_a_2=random_float(),
                escape_current_phase_a_3=random_float(),
                escape_current_phase_a_4=random_float(),
                escape_current_phase_b_1=random_float(),
                escape_current_phase_b_2=random_float(),
                escape_current_phase_b_3=random_float(),
                escape_current_phase_b_4=random_float(),
                escape_current_phase_c_1=random_float(),
                escape_current_phase_c_2=random_float(),
                escape_current_phase_c_3=random_float(),
                escape_current_phase_c_4=random_float(),
                temperature_1=random_float(),
                temperature_2=random_float(),
                temperature_3=random_float(),
                temperature_4=random_float(),
                temperature_5=random_float(),
                digital_output_1=True,
                digital_output_2=True,
                digital_output_3=True,
                digital_output_4=True,
            )
        else:
            s2 = SampleJson.objects.create(
                event=b,
                device=device,
                data={
                    'end_of_life_1': True,
                    'end_of_life_2': True,
                    'end_of_life_3': True,
                    'end_of_life_4': True,
                    'has_tension_1': True,
                    'has_tension_2': True,
                    'has_tension_3': True,
                    'has_tension_4': True,
                    'surge_tension_phase_a_1': random_float(),
                    'surge_tension_phase_a_2': random_float(),
                    'surge_tension_phase_a_3': random_float(),
                    'surge_tension_phase_a_4': random_float(),
                    'surge_tension_phase_b_1': random_float(),
                    'surge_tension_phase_b_2': random_float(),
                    'surge_tension_phase_b_3': random_float(),
                    'surge_tension_phase_b_4': random_float(),
                    'surge_tension_phase_c_1': random_float(),
                    'surge_tension_phase_c_2': random_float(),
                    'surge_tension_phase_c_3': random_float(),
                    'surge_tension_phase_c_4': random_float(),
                    'surge_current_phase_a_1': random_float(),
                    'surge_current_phase_a_2': random_float(),
                    'surge_current_phase_a_3': random_float(),
                    'surge_current_phase_a_4': random_float(),
                    'surge_current_phase_b_1': random_float(),
                    'surge_current_phase_b_2': random_float(),
                    'surge_current_phase_b_3': random_float(),
                    'surge_current_phase_b_4': random_float(),
                    'surge_current_phase_c_1': random_float(),
                    'surge_current_phase_c_2': random_float(),
                    'surge_current_phase_c_3': random_float(),
                    'surge_current_phase_c_4': random_float(),
                    'escape_current_phase_a_1': random_float(),
                    'escape_current_phase_a_2': random_float(),
                    'escape_current_phase_a_3': random_float(),
                    'escape_current_phase_a_4': random_float(),
                    'escape_current_phase_b_1': random_float(),
                    'escape_current_phase_b_2': random_float(),
                    'escape_current_phase_b_3': random_float(),
                    'escape_current_phase_b_4': random_float(),
                    'escape_current_phase_c_1': random_float(),
                    'escape_current_phase_c_2': random_float(),
                    'escape_current_phase_c_3': random_float(),
                    'escape_current_phase_c_4': random_float(),
                    'temperature_1': random_float(),
                    'temperature_2': random_float(),
                    'temperature_3': random_float(),
                    'temperature_4': random_float(),
                    'temperature_5': random_float(),
                    'digital_output_1': True,
                    'digital_output_2': True,
                    'digital_output_3': True,
                    'digital_output_4': True,
                }
            )


def gen_events(device):
    for index in range(100):
        # NotificationEvent.objects.create(
        #     event_type='eol',
        #     device=device,
        #     device_type=device.device_type
        # )

        a = MeasureableEvent.objects.create(
            event_type='overvo',
            device=device,
            device_type=device.model
        )
        b = MeasureableEvent.objects.create(
            event_type='steady',
            device=device,
            device_type=device.model
        )
        c = MeasureableVerticalEvent.objects.create(
            event_type='surge',
            device=device,
            device_type=device.model
        )

        s1 = Sample.objects.create(
            event=a,
            device=device,
            end_of_life_1=True,
            end_of_life_2=True,
            end_of_life_3=True,
            end_of_life_4=True,
            has_tension_1=True,
            has_tension_2=True,
            has_tension_3=True,
            has_tension_4=True,
            surge_tension_phase_a_1=random_float(),
            surge_tension_phase_a_2=random_float(),
            surge_tension_phase_a_3=random_float(),
            surge_tension_phase_a_4=random_float(),
            surge_tension_phase_b_1=random_float(),
            surge_tension_phase_b_2=random_float(),
            surge_tension_phase_b_3=random_float(),
            surge_tension_phase_b_4=random_float(),
            surge_tension_phase_c_1=random_float(),
            surge_tension_phase_c_2=random_float(),
            surge_tension_phase_c_3=random_float(),
            surge_tension_phase_c_4=random_float(),
            surge_current_phase_a_1=random_float(),
            surge_current_phase_a_2=random_float(),
            surge_current_phase_a_3=random_float(),
            surge_current_phase_a_4=random_float(),
            surge_current_phase_b_1=random_float(),
            surge_current_phase_b_2=random_float(),
            surge_current_phase_b_3=random_float(),
            surge_current_phase_b_4=random_float(),
            surge_current_phase_c_1=random_float(),
            surge_current_phase_c_2=random_float(),
            surge_current_phase_c_3=random_float(),
            surge_current_phase_c_4=random_float(),
            escape_current_phase_a_1=random_float(),
            escape_current_phase_a_2=random_float(),
            escape_current_phase_a_3=random_float(),
            escape_current_phase_a_4=random_float(),
            escape_current_phase_b_1=random_float(),
            escape_current_phase_b_2=random_float(),
            escape_current_phase_b_3=random_float(),
            escape_current_phase_b_4=random_float(),
            escape_current_phase_c_1=random_float(),
            escape_current_phase_c_2=random_float(),
            escape_current_phase_c_3=random_float(),
            escape_current_phase_c_4=random_float(),
            temperature_1=random_float(),
            temperature_2=random_float(),
            temperature_3=random_float(),
            temperature_4=random_float(),
            temperature_5=random_float(),
            digital_output_1=True,
            digital_output_2=True,
            digital_output_3=True,
            digital_output_4=True,
        )
        s2 = SampleJson.objects.create(
            event=b,
            device=device,
            data={
                'end_of_life_1': True,
                'end_of_life_2': True,
                'end_of_life_3': True,
                'end_of_life_4': True,
                'has_tension_1': True,
                'has_tension_2': True,
                'has_tension_3': True,
                'has_tension_4': True,
                'surge_tension_phase_a_1': random_float(),
                'surge_tension_phase_a_2': random_float(),
                'surge_tension_phase_a_3': random_float(),
                'surge_tension_phase_a_4': random_float(),
                'surge_tension_phase_b_1': random_float(),
                'surge_tension_phase_b_2': random_float(),
                'surge_tension_phase_b_3': random_float(),
                'surge_tension_phase_b_4': random_float(),
                'surge_tension_phase_c_1': random_float(),
                'surge_tension_phase_c_2': random_float(),
                'surge_tension_phase_c_3': random_float(),
                'surge_tension_phase_c_4': random_float(),
                'surge_current_phase_a_1': random_float(),
                'surge_current_phase_a_2': random_float(),
                'surge_current_phase_a_3': random_float(),
                'surge_current_phase_a_4': random_float(),
                'surge_current_phase_b_1': random_float(),
                'surge_current_phase_b_2': random_float(),
                'surge_current_phase_b_3': random_float(),
                'surge_current_phase_b_4': random_float(),
                'surge_current_phase_c_1': random_float(),
                'surge_current_phase_c_2': random_float(),
                'surge_current_phase_c_3': random_float(),
                'surge_current_phase_c_4': random_float(),
                'escape_current_phase_a_1': random_float(),
                'escape_current_phase_a_2': random_float(),
                'escape_current_phase_a_3': random_float(),
                'escape_current_phase_a_4': random_float(),
                'escape_current_phase_b_1': random_float(),
                'escape_current_phase_b_2': random_float(),
                'escape_current_phase_b_3': random_float(),
                'escape_current_phase_b_4': random_float(),
                'escape_current_phase_c_1': random_float(),
                'escape_current_phase_c_2': random_float(),
                'escape_current_phase_c_3': random_float(),
                'escape_current_phase_c_4': random_float(),
                'temperature_1': random_float(),
                'temperature_2': random_float(),
                'temperature_3': random_float(),
                'temperature_4': random_float(),
                'temperature_5': random_float(),
                'digital_output_1': True,
                'digital_output_2': True,
                'digital_output_3': True,
                'digital_output_4': True,
            }
        )

        fields = [
            {'end_of_life_1': True},
            {'end_of_life_2': True},
            {'end_of_life_3': True},
            {'end_of_life_4': True},
            {'has_tension_1': True},
            {'has_tension_2': True},
            {'has_tension_3': True},
            {'has_tension_4': True},
            {'surge_tension_phase_a_1': random_float()},
            {'surge_tension_phase_a_2': random_float()},
            {'surge_tension_phase_a_3': random_float()},
            {'surge_tension_phase_a_4': random_float()},
            {'surge_tension_phase_b_1': random_float()},
            {'surge_tension_phase_b_2': random_float()},
            {'surge_tension_phase_b_3': random_float()},
            {'surge_tension_phase_b_4': random_float()},
            {'surge_tension_phase_c_1': random_float()},
            {'surge_tension_phase_c_2': random_float()},
            {'surge_tension_phase_c_3': random_float()},
            {'surge_tension_phase_c_4': random_float()},
            {'surge_current_phase_a_1': random_float()},
            {'surge_current_phase_a_2': random_float()},
            {'surge_current_phase_a_3': random_float()},
            {'surge_current_phase_a_4': random_float()},
            {'surge_current_phase_b_1': random_float()},
            {'surge_current_phase_b_2': random_float()},
            {'surge_current_phase_b_3': random_float()},
            {'surge_current_phase_b_4': random_float()},
            {'surge_current_phase_c_1': random_float()},
            {'surge_current_phase_c_2': random_float()},
            {'surge_current_phase_c_3': random_float()},
            {'surge_current_phase_c_4': random_float()},
            {'escape_current_phase_a_1': random_float()},
            {'escape_current_phase_a_2': random_float()},
            {'escape_current_phase_a_3': random_float()},
            {'escape_current_phase_a_4': random_float()},
            {'escape_current_phase_b_1': random_float()},
            {'escape_current_phase_b_2': random_float()},
            {'escape_current_phase_b_3': random_float()},
            {'escape_current_phase_b_4': random_float()},
            {'escape_current_phase_c_1': random_float()},
            {'escape_current_phase_c_2': random_float()},
            {'escape_current_phase_c_3': random_float()},
            {'escape_current_phase_c_4': random_float()},
            {'temperature_1': random_float()},
            {'temperature_2': random_float()},
            {'temperature_3': random_float()},
            {'temperature_4': random_float()},
            {'temperature_5': random_float()},
            {'digital_output_1': True},
            {'digital_output_2': True},
            {'digital_output_3': True},
            {'digital_output_4': True},
        ]
        for f in fields:
            va = VerticalSample.objects.create(
                event=c,
                device=device,
                data=f,
            )

        print('gen_events nº %d, nº %d, nº %d' %
              (a.id, b.id, c.id))
def gen_events(device):
    for index in range(10):
        # NotificationEvent.objects.create(
        #     event_type='eol',
        #     device=device,
        #     device_type=device.device_type
        # )

        a = MeasureableVerticalEvent.objects.create(
            event_type='overvo',
            device=device,
            device_type=device.model
        )
        b = MeasureableVerticalEvent.objects.create(
            event_type='steady',
            device=device,
            device_type=device.model
        )
        c = MeasureableVerticalEvent.objects.create(
            event_type='surge',
            device=device,
            device_type=device.model
        )

        fields = [
            {'end_of_life_1': True},
            {'end_of_life_2': True},
            {'end_of_life_3': True},
            {'end_of_life_4': True},
            {'has_tension_1': True},
            {'has_tension_2': True},
            {'has_tension_3': True},
            {'has_tension_4': True},
            {'surge_tension_phase_a_1': random_float()},
            {'surge_tension_phase_a_2': random_float()},
            {'surge_tension_phase_a_3': random_float()},
            {'surge_tension_phase_a_4': random_float()},
            {'surge_tension_phase_b_1': random_float()},
            {'surge_tension_phase_b_2': random_float()},
            {'surge_tension_phase_b_3': random_float()},
            {'surge_tension_phase_b_4': random_float()},
            {'surge_tension_phase_c_1': random_float()},
            {'surge_tension_phase_c_2': random_float()},
            {'surge_tension_phase_c_3': random_float()},
            {'surge_tension_phase_c_4': random_float()},
            {'surge_current_phase_a_1': random_float()},
            {'surge_current_phase_a_2': random_float()},
            {'surge_current_phase_a_3': random_float()},
            {'surge_current_phase_a_4': random_float()},
            {'surge_current_phase_b_1': random_float()},
            {'surge_current_phase_b_2': random_float()},
            {'surge_current_phase_b_3': random_float()},
            {'surge_current_phase_b_4': random_float()},
            {'surge_current_phase_c_1': random_float()},
            {'surge_current_phase_c_2': random_float()},
            {'surge_current_phase_c_3': random_float()},
            {'surge_current_phase_c_4': random_float()},
            {'escape_current_phase_a_1': random_float()},
            {'escape_current_phase_a_2': random_float()},
            {'escape_current_phase_a_3': random_float()},
            {'escape_current_phase_a_4': random_float()},
            {'escape_current_phase_b_1': random_float()},
            {'escape_current_phase_b_2': random_float()},
            {'escape_current_phase_b_3': random_float()},
            {'escape_current_phase_b_4': random_float()},
            {'escape_current_phase_c_1': random_float()},
            {'escape_current_phase_c_2': random_float()},
            {'escape_current_phase_c_3': random_float()},
            {'escape_current_phase_c_4': random_float()},
            {'temperature_1': random_float()},
            {'temperature_2': random_float()},
            {'temperature_3': random_float()},
            {'temperature_4': random_float()},
            {'temperature_5': random_float()},
            {'digital_output_1': True},
            {'digital_output_2': True},
            {'digital_output_3': True},
            {'digital_output_4': True},
        ]
        for f in fields:
            va = VerticalSample.objects.create(
                event=a,
                device=device,
                data=f,
            )
            vb = VerticalSample.objects.create(
                event=b,
                device=device,
                data=f,
            )
            vc = VerticalSample.objects.create(
                event=c,
                device=device,
                data=f,
            )

        print('gen_events nº %d, nº %d, nº %d' %
              (a.id, b.id, c.id))


def fill_hybrid(devices):
    for device in devices:
        events = [
            MeasureableEvent.objects.create(
                device=device,
                event_type='steady'
            ),
            MeasureableEvent.objects.create(
                device=device,
                event_type='steady'
            ),
            MeasureableEvent.objects.create(
                device=device,
                event_type='steady'
            ),
            MeasureableEvent.objects.create(
                device=device,
                event_type='steady'
            ),
            MeasureableEvent.objects.create(
                device=device,
                event_type='steady'
            ),
        ]
        for ev in events:
            a = HybridSample.objects.create(
                a=random_float(),
                b=random_float(),
                c=random_float(),
                d=random_float(),
            )
            b = HybridSample.objects.create(
                a=random_float(),
                b=random_float(),
                c=random_float(),
                d=random_float(),
            )
            print('Created hybrids #%d and #%d for event #%d' % (a.id, b.id, ev.id))




if __name__ == "__main__":
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dbtest.settings')
    django.setup()

    from dbtest.events.models import *
    from dbtest.devices.models import *
    from dbtest.samples.models import *
    from dbtest.device_types.models import *

    devices = Device.objects.filter()

    tax = round(len(devices) / 2)
    dev_a = devices[:tax]
    dev_b = devices[tax:]

    alt_a, alt_b = dev_a, dev_b

    dev_c, dev_a = alt_a[:round(len(dev_a) / 2)], alt_a[round(len(dev_a) / 2):]
    dev_d, dev_b = alt_b[:round(len(dev_b) / 2)], alt_b[round(len(dev_b) / 2):]

    a = threading.Thread(target=fill_hybrid, args=(dev_a,))
    b = threading.Thread(target=fill_hybrid, args=(dev_b,))
    c = threading.Thread(target=fill_hybrid, args=(dev_c,))
    d = threading.Thread(target=fill_hybrid, args=(dev_d,))

    a.start()
    b.start()
    c.start()
    d.start()

    a.join()
    b.join()
    c.join()
    d.join()

    devices = Device.objects.fil
    print('Finished everything')
