Device -> 4000 objects
DeviceType -> 1 objects
NotificationEvent -> 42900 objects
MeasureableEvent -> 171600 objects
MeasureableVerticalEvent -> 341396 objects
Sample -> 171600 objects
VerticalSample -> 9047142 objects
AltVerticalSample -> 1194822 objects

Using 50 devices, since they have associated events
-------------------------------------------------------------------
Results (ran "get_all_query" for each model 25 times):
Cmd: Model.objects.all()

Sample:
 - average:   11.350605s

VerticalSample:
 - average:   131.554684s

AltVerticalSample:
 - average:   36.506704s
-------------------------------------------------------------------
Results (ran "get_filter_query_first_device" for each model 25 times):
Cmd: Model.objects.filter(event=event)

Sample:
 - results: [0.0020523166656494143]
 - best:      0.002052s
 - worst:     0.002052s
 - average:   0.002052s

VerticalSample:
 - results: [0.0016531753540039063]
 - best:      0.001653s
 - worst:     0.001653s
 - average:   0.001653s

AltVerticalSample:
 results: [0.0016820049285888672]
 - best:      0.001682s
 - worst:     0.001682s
 - average:   0.001682s
-------------------------------------------------------------------
Results (ran "get_filter_query_middle_device" for each model 25 times):
Cmd: Model.objects.filter(event=event)

Sample:
 - results: [0.00226959228515625]
 - best:      0.002270s
 - worst:     0.002270s
 - average:   0.002270s

VerticalSample:
 - results: [0.0016996097564697266]
 - best:      0.001700s
 - worst:     0.001700s
 - average:   0.001700s

AltVerticalSample:
 results: [0.0016904067993164062]
 - best:      0.001690s
 - worst:     0.001690s
 - average:   0.001690s
-------------------------------------------------------------------
Results (ran "get_filter_query_last_device" for each model 25 times):
Cmd: Model.objects.filter(event=event)

Sample:
 - results: [0.0019495487213134766]
 - best:      0.001950s
 - worst:     0.001950s
 - average:   0.001950s

VerticalSample:
 - results: [0.0016566753387451173]
 - best:      0.001657s
 - worst:     0.001657s
 - average:   0.001657s

AltVerticalSample:
 results: [0.0016713333129882812]
 - best:      0.001671s
 - worst:     0.001671s
 - average:   0.001671s
-------------------------------------------------------------------
Results (ran "get_device_samples_through_events_first_device" for each model 25 times):
Cmd: device.[measureable_events | measureable_vertical_events].[first | last]().[sample | v_samples.all()]

Sample:
 - results: [0.003827056884765625, 0.10873937606811523]
 - best:      0.003827s
 - worst:     0.108739s
 - average:   0.056283s

VerticalSample:
 - results: [0.004929780960083008, 0.18718281745910645]
 - best:      0.004930s
 - worst:     0.187183s
 - average:   0.096056s

AltVerticalSample:
 results: [0.003448753356933594, 0.11403956413269042]
 - best:      0.003449s
 - worst:     0.114040s
 - average:   0.058744s
-------------------------------------------------------------------
Results (ran "get_device_samples_through_events_middle_device" for each model 25 times):
Cmd: device.[measureable_events | measureable_vertical_events].[first | last]().[sample | v_samples.all()]

Sample:
 - results: [0.027814435958862304, 0.057161865234375]
 - best:      0.027814s
 - worst:     0.057162s
 - average:   0.042488s

VerticalSample:
 - results: [0.03926909446716308, 0.08036452293395996]
 - best:      0.039269s
 - worst:     0.080365s
 - average:   0.059817s

AltVerticalSample:
 results: [0.039173555374145505, 0.08145123481750488]
 - best:      0.039174s
 - worst:     0.081451s
 - average:   0.060312s
-------------------------------------------------------------------
Results (ran "get_device_samples_through_events_last_device" for each model 25 times):
Cmd: device.[measureable_events | measureable_vertical_events].[first | last]().[sample | v_samples.all()]

Sample:
 - results: [0.05186727523803711, 0.034332408905029296]
 - best:      0.034332s
 - worst:     0.051867s
 - average:   0.043100s

VerticalSample:
 - results: [0.07766048431396484, 0.047616920471191405]
 - best:      0.047617s
 - worst:     0.077660s
 - average:   0.062639s

AltVerticalSample:
 results: [0.07355390548706055, 0.04557682991027832]
 - best:      0.045577s
 - worst:     0.073554s
 - average:   0.059565s
