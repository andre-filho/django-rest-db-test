from django.db import models
from django.contrib.postgres.fields import JSONField

from dbtest.devices.models import Device
from dbtest.events.models import MeasureableEvent
from dbtest.events.models import MeasureableVerticalEvent


class AbstractSample(models.Model):
    event = models.OneToOneField(
        MeasureableEvent, related_name='sample', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        abstract = True


class Sample(AbstractSample):
    event = models.OneToOneField(
        MeasureableEvent, related_name='sample', on_delete=models.CASCADE, blank=True, null=True)

    surge_tension_phase_a_1 = models.FloatField(blank=True, null=True)
    surge_tension_phase_a_2 = models.FloatField(blank=True, null=True)
    surge_tension_phase_a_3 = models.FloatField(blank=True, null=True)
    surge_tension_phase_a_4 = models.FloatField(blank=True, null=True)

    surge_tension_phase_b_1 = models.FloatField(blank=True, null=True)
    surge_tension_phase_b_2 = models.FloatField(blank=True, null=True)
    surge_tension_phase_b_3 = models.FloatField(blank=True, null=True)
    surge_tension_phase_b_4 = models.FloatField(blank=True, null=True)

    surge_tension_phase_c_1 = models.FloatField(blank=True, null=True)
    surge_tension_phase_c_2 = models.FloatField(blank=True, null=True)
    surge_tension_phase_c_3 = models.FloatField(blank=True, null=True)
    surge_tension_phase_c_4 = models.FloatField(blank=True, null=True)

    surge_current_phase_a_1 = models.FloatField(blank=True, null=True)
    surge_current_phase_a_2 = models.FloatField(blank=True, null=True)
    surge_current_phase_a_3 = models.FloatField(blank=True, null=True)
    surge_current_phase_a_4 = models.FloatField(blank=True, null=True)

    surge_current_phase_b_1 = models.FloatField(blank=True, null=True)
    surge_current_phase_b_2 = models.FloatField(blank=True, null=True)
    surge_current_phase_b_3 = models.FloatField(blank=True, null=True)
    surge_current_phase_b_4 = models.FloatField(blank=True, null=True)

    surge_current_phase_c_1 = models.FloatField(blank=True, null=True)
    surge_current_phase_c_2 = models.FloatField(blank=True, null=True)
    surge_current_phase_c_3 = models.FloatField(blank=True, null=True)
    surge_current_phase_c_4 = models.FloatField(blank=True, null=True)

    escape_current_phase_a_1 = models.FloatField(blank=True, null=True)
    escape_current_phase_a_2 = models.FloatField(blank=True, null=True)
    escape_current_phase_a_3 = models.FloatField(blank=True, null=True)
    escape_current_phase_a_4 = models.FloatField(blank=True, null=True)

    escape_current_phase_b_1 = models.FloatField(blank=True, null=True)
    escape_current_phase_b_2 = models.FloatField(blank=True, null=True)
    escape_current_phase_b_3 = models.FloatField(blank=True, null=True)
    escape_current_phase_b_4 = models.FloatField(blank=True, null=True)

    escape_current_phase_c_1 = models.FloatField(blank=True, null=True)
    escape_current_phase_c_2 = models.FloatField(blank=True, null=True)
    escape_current_phase_c_3 = models.FloatField(blank=True, null=True)
    escape_current_phase_c_4 = models.FloatField(blank=True, null=True)

    temperature_1 = models.FloatField(blank=True, null=True)
    temperature_2 = models.FloatField(blank=True, null=True)
    temperature_3 = models.FloatField(blank=True, null=True)
    temperature_4 = models.FloatField(blank=True, null=True)
    temperature_5 = models.FloatField(blank=True, null=True)


class SampleJson(AbstractSample):
    event = models.OneToOneField(
        MeasureableEvent, related_name='jsample', on_delete=models.CASCADE, blank=True, null=True)

    data = JSONField()

class VerticalSample(models.Model):
    event = models.ForeignKey(
        MeasureableVerticalEvent, related_name='samples', on_delete=models.CASCADE, blank=True, null=True)
    field = models.CharField(max_length=60)
    data = models.FloatField()


class AltVerticalSample(models.Model):
    event = models.ForeignKey(
        MeasureableVerticalEvent, related_name='altvsamples', on_delete=models.CASCADE, blank=True, null=True)
    field = models.CharField(max_length=60)
    data = JSONField()


class HybridSample(models.Model):
    event = models.ForeignKey(
        MeasureableEvent, related_name='hsamples', on_delete=models.CASCADE)
    a = models.FloatField(blank=True, null=True)
    b = models.FloatField(blank=True, null=True)
    c = models.FloatField(blank=True, null=True)
    d = models.FloatField(blank=True, null=True)
