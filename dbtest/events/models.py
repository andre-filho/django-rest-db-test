from django.db import models
from dbtest.devices.models import Device
from dbtest.device_types.models import DeviceType


class Event(models.Model):
    device = models.ForeignKey(Device, related_name='events', on_delete=models.CASCADE, blank=True, null=True)
    device_type = models.ForeignKey(DeviceType, on_delete=models.CASCADE, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


NOTIFICATION_EVENT_CHOICES = [
    ('eol', 'end of life'),  # end of life
]


class NotificationEvent(Event):
    device = models.ForeignKey(
        Device, related_name='notification_events', on_delete=models.CASCADE, blank=True, null=True)
    event_type = models.CharField(max_length=6, choices=NOTIFICATION_EVENT_CHOICES)


MEASUREABLE_EVENT_CHOICES = [
    ('overvo', 'over_voltage'),
    ('steady', 'steady_state'),
    ('surge', 'surge'),
]


class MeasureableEvent(Event):
    device = models.ForeignKey(
        Device, related_name='measureable_events', on_delete=models.CASCADE, blank=True, null=True)
    event_type = models.CharField(max_length=6, choices=MEASUREABLE_EVENT_CHOICES)


class MeasureableVerticalEvent(Event):
    device = models.ForeignKey(
        Device, related_name='measureable_vertical_events', on_delete=models.CASCADE, blank=True, null=True)
    event_type = models.CharField(
        max_length=6, choices=MEASUREABLE_EVENT_CHOICES)
