from django.apps import AppConfig


class DeviceTypesConfig(AppConfig):
    name = 'device_types'
