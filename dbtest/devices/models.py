from django.db import models
from datetime import datetime
from dbtest.device_types.models import DeviceType


class Device(models.Model):
    device_type = models.ForeignKey(DeviceType, on_delete=models.CASCADE)
    serial_number = models.CharField(max_length=255, unique=True)
    mac_address = models.CharField(max_length=255, unique=True, null=True)
    model = models.CharField(max_length=255, null=True, blank=True)
    geographic_location = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    lifespan = models.FloatField(default=100)
