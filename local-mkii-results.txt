Device -> 20 objects
DeviceType -> 1 objects
NotificationEvent -> 1300 objects
MeasureableEvent -> 5200 objects
MeasureableVerticalEvent -> 10400 objects
Sample -> 5200 objects
VerticalSample -> 213200 objects
AltVerticalSample -> 20800 objects

Using 50 devices, since they have associated events
-------------------------------------------------------------------
Results (ran "get_all_query" for each model 20 times):

Sample:
 - best:      0.440313s
 - worst:     0.504016s
 - average:   0.464127s

VerticalSample:
 - best:      3.975187s
 - worst:     4.617522s
 - average:   4.122807s

AltVerticalSample:
 - best:      0.805933s
 - worst:     1.051002s
 - average:   0.888434s
Traceback (most recent call last):
  File "test_queries.py", line 173, in <module>
    res.append(get_filter_query(Sample, first_device))
  File "test_queries.py", line 69, in get_filter_query
    event = query[random.randint(0, len(query) - 1)]
  File "/usr/local/lib/python3.6/random.py", line 221, in randint
    return self.randrange(a, b+1)
  File "/usr/local/lib/python3.6/random.py", line 199, in randrange
    raise ValueError("empty range for randrange() (%d,%d, %d)" % (istart, istop, width))
ValueError: empty range for randrange() (0,0, 0)
