Device -> 4000 objects
DeviceType -> 1 objects
NotificationEvent -> 42900 objects
MeasureableEvent -> 171600 objects
MeasureableVerticalEvent -> 341396 objects
Sample -> 171600 objects
VerticalSample -> 9047142 objects
AltVerticalSample -> 1194822 objects

Using 50 devices, since they have associated events
-------------------------------------------------------------------
Results (ran "get_all_query" for each model 25 times):
Cmd: Model.objects.all()

Sample:
 - average:   11.424272s

VerticalSample:
 - average:   133.563719s

AltVerticalSample:
 - average:   37.128448s
-------------------------------------------------------------------
Results (ran "get_filter_query_first_device" for each model 25 times):
Cmd: Model.objects.filter(event=event)

Sample:
 - results: [0.0019184207916259765]
 - best:      0.001918s
 - worst:     0.001918s
 - average:   0.001918s

VerticalSample:
 - results: [0.0016274070739746094]
 - best:      0.001627s
 - worst:     0.001627s
 - average:   0.001627s

AltVerticalSample:
 results: [0.0016459083557128905]
 - best:      0.001646s
 - worst:     0.001646s
 - average:   0.001646s
-------------------------------------------------------------------
Results (ran "get_filter_query_middle_device" for each model 25 times):
Cmd: Model.objects.filter(event=event)

Sample:
 - results: [0.0019362449645996093]
 - best:      0.001936s
 - worst:     0.001936s
 - average:   0.001936s

VerticalSample:
 - results: [0.0016523456573486327]
 - best:      0.001652s
 - worst:     0.001652s
 - average:   0.001652s

AltVerticalSample:
 results: [0.0016806411743164062]
 - best:      0.001681s
 - worst:     0.001681s
 - average:   0.001681s
-------------------------------------------------------------------
Results (ran "get_filter_query_last_device" for each model 25 times):
Cmd: Model.objects.filter(event=event)

Sample:
 - results: [0.001984977722167969]
 - best:      0.001985s
 - worst:     0.001985s
 - average:   0.001985s

VerticalSample:
 - results: [0.0018087291717529296]
 - best:      0.001809s
 - worst:     0.001809s
 - average:   0.001809s

AltVerticalSample:
 results: [0.0016823291778564453]
 - best:      0.001682s
 - worst:     0.001682s
 - average:   0.001682s
-------------------------------------------------------------------
Results (ran "get_device_samples_through_events_first_device" for each model 25 times):
Cmd: device.[measureable_events | measureable_vertical_events].[first | last]().[sample | v_samples.all()]

Sample:
 - results: [0.0040384578704833985, 0.09334774971008301]
 - best:      0.004038s
 - worst:     0.093348s
 - average:   0.048693s

VerticalSample:
 - results: [0.0036248016357421876, 0.1454741859436035]
 - best:      0.003625s
 - worst:     0.145474s
 - average:   0.074549s

AltVerticalSample:
 results: [0.003494510650634766, 0.11382579803466797]
 - best:      0.003495s
 - worst:     0.113826s
 - average:   0.058660s
-------------------------------------------------------------------
Results (ran "get_device_samples_through_events_middle_device" for each model 25 times):
Cmd: device.[measureable_events | measureable_vertical_events].[first | last]().[sample | v_samples.all()]

Sample:
 - results: [0.027813434600830078, 0.05701200485229492]
 - best:      0.027813s
 - worst:     0.057012s
 - average:   0.042413s

VerticalSample:
 - results: [0.03922438621520996, 0.07941647529602051]
 - best:      0.039224s
 - worst:     0.079416s
 - average:   0.059320s

AltVerticalSample:
 results: [0.03901280403137207, 0.07962284088134766]
 - best:      0.039013s
 - worst:     0.079623s
 - average:   0.059318s
-------------------------------------------------------------------
Results (ran "get_device_samples_through_events_last_device" for each model 25 times):
Cmd: device.[measureable_events | measureable_vertical_events].[first | last]().[sample | v_samples.all()]

Sample:
 - results: [0.05156325340270996, 0.03352928161621094]
 - best:      0.033529s
 - worst:     0.051563s
 - average:   0.042546s

VerticalSample:
 - results: [0.07319003105163574, 0.04521903038024902]
 - best:      0.045219s
 - worst:     0.073190s
 - average:   0.059205s

AltVerticalSample:
 results: [0.07295369148254395, 0.045176458358764646]
 - best:      0.045176s
 - worst:     0.072954s
 - average:   0.059065s
