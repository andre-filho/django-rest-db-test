def random_float():
    rand = random.random()
    return random.uniform(rand, rand*15)


def random_int():
    return random.randint(800, 1500)
    # return 1500


def gen_events(device):
    # for index in range(1):
    for index in range(65):
        NotificationEvent.objects.create(
            event_type='eol',
            device=device,
            device_type=device.device_type
        )

        a = MeasureableEvent.objects.create(
            event_type='overvo',
            device=device,
            device_type=device.model
        )
        b = MeasureableEvent.objects.create(
            event_type='steady',
            device=device,
            device_type=device.model
        )
        c = MeasureableEvent.objects.create(
            event_type='surge',
            device=device,
            device_type=device.model
        )
        d = MeasureableEvent.objects.create(
            event_type='overvo',
            device=device,
            device_type=device.model
        )

        s1 = Sample.objects.create(
            event=a,
            surge_tension_phase_a_1=random_float(),
            surge_tension_phase_a_2=random_float(),
            surge_tension_phase_a_3=random_float(),
            surge_tension_phase_a_4=random_float(),
            surge_tension_phase_b_1=random_float(),
            surge_tension_phase_b_2=random_float(),
            surge_tension_phase_b_3=random_float(),
            surge_tension_phase_b_4=random_float(),
            surge_tension_phase_c_1=random_float(),
            surge_tension_phase_c_2=random_float(),
            surge_tension_phase_c_3=random_float(),
            surge_tension_phase_c_4=random_float(),
            surge_current_phase_a_1=random_float(),
            surge_current_phase_a_2=random_float(),
            surge_current_phase_a_3=random_float(),
            surge_current_phase_a_4=random_float(),
            surge_current_phase_b_1=random_float(),
            surge_current_phase_b_2=random_float(),
            surge_current_phase_b_3=random_float(),
            surge_current_phase_b_4=random_float(),
            surge_current_phase_c_1=random_float(),
            surge_current_phase_c_2=random_float(),
            surge_current_phase_c_3=random_float(),
            surge_current_phase_c_4=random_float(),
            escape_current_phase_a_1=random_float(),
            escape_current_phase_a_2=random_float(),
            escape_current_phase_a_3=random_float(),
            escape_current_phase_a_4=random_float(),
            escape_current_phase_b_1=random_float(),
            escape_current_phase_b_2=random_float(),
            escape_current_phase_b_3=random_float(),
            escape_current_phase_b_4=random_float(),
            escape_current_phase_c_1=random_float(),
            escape_current_phase_c_2=random_float(),
            escape_current_phase_c_3=random_float(),
            escape_current_phase_c_4=random_float(),
            temperature_1=random_float(),
            temperature_2=random_float(),
            temperature_3=random_float(),
            temperature_4=random_float(),
            temperature_5=random_float(),
        )
        s2 = Sample.objects.create(
            event=b,
            surge_tension_phase_a_1=random_float(),
            surge_tension_phase_a_2=random_float(),
            surge_tension_phase_a_3=random_float(),
            surge_tension_phase_a_4=random_float(),
            surge_tension_phase_b_1=random_float(),
            surge_tension_phase_b_2=random_float(),
            surge_tension_phase_b_3=random_float(),
            surge_tension_phase_b_4=random_float(),
            surge_tension_phase_c_1=random_float(),
            surge_tension_phase_c_2=random_float(),
            surge_tension_phase_c_3=random_float(),
            surge_tension_phase_c_4=random_float(),
            surge_current_phase_a_1=random_float(),
            surge_current_phase_a_2=random_float(),
            surge_current_phase_a_3=random_float(),
            surge_current_phase_a_4=random_float(),
            surge_current_phase_b_1=random_float(),
            surge_current_phase_b_2=random_float(),
            surge_current_phase_b_3=random_float(),
            surge_current_phase_b_4=random_float(),
            surge_current_phase_c_1=random_float(),
            surge_current_phase_c_2=random_float(),
            surge_current_phase_c_3=random_float(),
            surge_current_phase_c_4=random_float(),
            escape_current_phase_a_1=random_float(),
            escape_current_phase_a_2=random_float(),
            escape_current_phase_a_3=random_float(),
            escape_current_phase_a_4=random_float(),
            escape_current_phase_b_1=random_float(),
            escape_current_phase_b_2=random_float(),
            escape_current_phase_b_3=random_float(),
            escape_current_phase_b_4=random_float(),
            escape_current_phase_c_1=random_float(),
            escape_current_phase_c_2=random_float(),
            escape_current_phase_c_3=random_float(),
            escape_current_phase_c_4=random_float(),
            temperature_1=random_float(),
            temperature_2=random_float(),
            temperature_3=random_float(),
            temperature_4=random_float(),
            temperature_5=random_float(),
        )
        s3 = Sample.objects.create(
            event=c,
            surge_tension_phase_a_1=random_float(),
            surge_tension_phase_a_2=random_float(),
            surge_tension_phase_a_3=random_float(),
            surge_tension_phase_a_4=random_float(),
            surge_tension_phase_b_1=random_float(),
            surge_tension_phase_b_2=random_float(),
            surge_tension_phase_b_3=random_float(),
            surge_tension_phase_b_4=random_float(),
            surge_tension_phase_c_1=random_float(),
            surge_tension_phase_c_2=random_float(),
            surge_tension_phase_c_3=random_float(),
            surge_tension_phase_c_4=random_float(),
            surge_current_phase_a_1=random_float(),
            surge_current_phase_a_2=random_float(),
            surge_current_phase_a_3=random_float(),
            surge_current_phase_a_4=random_float(),
            surge_current_phase_b_1=random_float(),
            surge_current_phase_b_2=random_float(),
            surge_current_phase_b_3=random_float(),
            surge_current_phase_b_4=random_float(),
            surge_current_phase_c_1=random_float(),
            surge_current_phase_c_2=random_float(),
            surge_current_phase_c_3=random_float(),
            surge_current_phase_c_4=random_float(),
            escape_current_phase_a_1=random_float(),
            escape_current_phase_a_2=random_float(),
            escape_current_phase_a_3=random_float(),
            escape_current_phase_a_4=random_float(),
            escape_current_phase_b_1=random_float(),
            escape_current_phase_b_2=random_float(),
            escape_current_phase_b_3=random_float(),
            escape_current_phase_b_4=random_float(),
            escape_current_phase_c_1=random_float(),
            escape_current_phase_c_2=random_float(),
            escape_current_phase_c_3=random_float(),
            escape_current_phase_c_4=random_float(),
            temperature_1=random_float(),
            temperature_2=random_float(),
            temperature_3=random_float(),
            temperature_4=random_float(),
            temperature_5=random_float(),
        )
        s4 = Sample.objects.create(
            event=d,
            surge_tension_phase_a_1=random_float(),
            surge_tension_phase_a_2=random_float(),
            surge_tension_phase_a_3=random_float(),
            surge_tension_phase_a_4=random_float(),
            surge_tension_phase_b_1=random_float(),
            surge_tension_phase_b_2=random_float(),
            surge_tension_phase_b_3=random_float(),
            surge_tension_phase_b_4=random_float(),
            surge_tension_phase_c_1=random_float(),
            surge_tension_phase_c_2=random_float(),
            surge_tension_phase_c_3=random_float(),
            surge_tension_phase_c_4=random_float(),
            surge_current_phase_a_1=random_float(),
            surge_current_phase_a_2=random_float(),
            surge_current_phase_a_3=random_float(),
            surge_current_phase_a_4=random_float(),
            surge_current_phase_b_1=random_float(),
            surge_current_phase_b_2=random_float(),
            surge_current_phase_b_3=random_float(),
            surge_current_phase_b_4=random_float(),
            surge_current_phase_c_1=random_float(),
            surge_current_phase_c_2=random_float(),
            surge_current_phase_c_3=random_float(),
            surge_current_phase_c_4=random_float(),
            escape_current_phase_a_1=random_float(),
            escape_current_phase_a_2=random_float(),
            escape_current_phase_a_3=random_float(),
            escape_current_phase_a_4=random_float(),
            escape_current_phase_b_1=random_float(),
            escape_current_phase_b_2=random_float(),
            escape_current_phase_b_3=random_float(),
            escape_current_phase_b_4=random_float(),
            escape_current_phase_c_1=random_float(),
            escape_current_phase_c_2=random_float(),
            escape_current_phase_c_3=random_float(),
            escape_current_phase_c_4=random_float(),
            temperature_1=random_float(),
            temperature_2=random_float(),
            temperature_3=random_float(),
            temperature_4=random_float(),
            temperature_5=random_float(),
        )
        print('gen_events nº %d, nº %d, nº %d, nº %d' %
              (a.id, b.id, c.id, d.id))


def gen_vertical_events(device):
    # for index in range(1):
    for index in range(65):
        fields = [
            ['surge_tension_phase_a_1', random_float()],
            ['surge_tension_phase_a_2', random_float()],
            ['surge_tension_phase_a_3', random_float()],
            ['surge_tension_phase_a_4', random_float()],
            ['surge_tension_phase_b_1', random_float()],
            ['surge_tension_phase_b_2', random_float()],
            ['surge_tension_phase_b_3', random_float()],
            ['surge_tension_phase_b_4', random_float()],
            ['surge_tension_phase_c_1', random_float()],
            ['surge_tension_phase_c_2', random_float()],
            ['surge_tension_phase_c_3', random_float()],
            ['surge_tension_phase_c_4', random_float()],
            ['surge_current_phase_a_1', random_float()],
            ['surge_current_phase_a_2', random_float()],
            ['surge_current_phase_a_3', random_float()],
            ['surge_current_phase_a_4', random_float()],
            ['surge_current_phase_b_1', random_float()],
            ['surge_current_phase_b_2', random_float()],
            ['surge_current_phase_b_3', random_float()],
            ['surge_current_phase_b_4', random_float()],
            ['surge_current_phase_c_1', random_float()],
            ['surge_current_phase_c_2', random_float()],
            ['surge_current_phase_c_3', random_float()],
            ['surge_current_phase_c_4', random_float()],
            ['escape_current_phase_a_1', random_float()],
            ['escape_current_phase_a_2', random_float()],
            ['escape_current_phase_a_3', random_float()],
            ['escape_current_phase_a_4', random_float()],
            ['escape_current_phase_b_1', random_float()],
            ['escape_current_phase_b_2', random_float()],
            ['escape_current_phase_b_3', random_float()],
            ['escape_current_phase_b_4', random_float()],
            ['escape_current_phase_c_1', random_float()],
            ['escape_current_phase_c_2', random_float()],
            ['escape_current_phase_c_3', random_float()],
            ['escape_current_phase_c_4', random_float()],
            ['temperature_1', random_float()],
            ['temperature_2', random_float()],
            ['temperature_3', random_float()],
            ['temperature_4', random_float()],
            ['temperature_5', random_float()],
        ]


        a = MeasureableVerticalEvent.objects.create(
            event_type='overvo',
            device=device,
            device_type=device.model
        )
        b = MeasureableVerticalEvent.objects.create(
            event_type='steady',
            device=device,
            device_type=device.model
        )
        c = MeasureableVerticalEvent.objects.create(
            event_type='surge',
            device=device,
            device_type=device.model
        )
        d = MeasureableVerticalEvent.objects.create(
            event_type='overvo',
            device=device,
            device_type=device.model
        )

        for field in fields:
            va = VerticalSample.objects.create(
                event=a,
                field=field[0],
                data=field[1]
            )
            vb = VerticalSample.objects.create(
                event=b,
                field=field[0],
                data=field[1],
            )
            vc = VerticalSample.objects.create(
                event=c,
                field=field[0],
                data=field[1],
            )
            vd = VerticalSample.objects.create(
                event=d,
                field=field[0],
                data=field[1],
            )
            print('Created vertical samples nº %d, nº %d, nº %d, nº %d' %
                (va.id, vb.id, vc.id, vd.id))

        fields = [
            ['surge_tension', {
                'a': [random_float(), random_float(), random_float()],
                'b': [random_float(), random_float(), random_float()],
                'c': [random_float(), random_float(), random_float()],
                'd': [random_float(), random_float(), random_float()]
            }],

            ['surge_current', {
                'a': [random_float(), random_float(), random_float()],
                'b': [random_float(), random_float(), random_float()],
                'c': [random_float(), random_float(), random_float()],
                'd': [random_float(), random_float(), random_float()]
            }],

            ['escape_current', {
                'a': [random_float(), random_float(), random_float()],
                'b': [random_float(), random_float(), random_float()],
                'c': [random_float(), random_float(), random_float()],
                'd': [random_float(), random_float(), random_float()]
            }],

            ['temperature', {
                'int': [
                    random_float(),
                    random_float(),
                    random_float(),
                    random_float()
                ],
                'ext': random_float()
            }],
        ]

        xa = MeasureableVerticalEvent.objects.create(
            event_type='overvo',
            device=device,
            device_type=device.model
        )
        xb = MeasureableVerticalEvent.objects.create(
            event_type='overvo',
            device=device,
            device_type=device.model
        )
        xc = MeasureableVerticalEvent.objects.create(
            event_type='overvo',
            device=device,
            device_type=device.model
        )
        xd = MeasureableVerticalEvent.objects.create(
            event_type='overvo',
            device=device,
            device_type=device.model
        )

        for field in fields:
            va = AltVerticalSample.objects.create(
                event=xa,
                field=field[0],
                data=field[1],
            )
            vb = AltVerticalSample.objects.create(
                event=xb,
                field=field[0],
                data=field[1],
            )
            vc = AltVerticalSample.objects.create(
                event=xc,
                field=field[0],
                data=field[1],
            )
            vd = AltVerticalSample.objects.create(
                event=xd,
                field=field[0],
                data=field[1],
            )
            print('Created alt vertical samples nº %d, nº %d, nº %d, nº %d' %
                (va.id, vb.id, vc.id, vd.id))



def gen_mac_address():
    return '%d:%d:%d:%d' % (random_int(), random_int(), random_int(), random_int())


def gen_devices(typ):
    # for index in range(1):
    for index in range(5):
        ax = Device.objects.create(
            serial_number=gen_mac_address(),
            mac_address=gen_mac_address(),
            device_type=typ,
            geographic_location='fdssg',
        )
        print('gen_devices: %d' % ax.id)
    print('thread is finished!')


def pop_events(devices):
    for device in devices:
        print('poping events for device %d' % device.id)
        gen_events(device)
        gen_vertical_events(device)
    print('thread is finished!')


if __name__ == "__main__":
    import os
    import django
    import sys
    import random

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dbtest.settings')
    django.setup()

    from dbtest.events.models import *
    from dbtest.samples.models import *
    from dbtest.devices.models import *
    from dbtest.device_types.models import *

    import threading

    typ = DeviceType.objects.create(name='model#6')

    a = threading.Thread(target=gen_devices, args=(typ,))
    b = threading.Thread(target=gen_devices, args=(typ,))
    c = threading.Thread(target=gen_devices, args=(typ,))
    d = threading.Thread(target=gen_devices, args=(typ,))

    print('----------------------------')
    print('Starting gen_devices threads!')
    print('----------------------------')

    a.start()
    b.start()
    c.start()
    d.start()

    a.join()
    b.join()
    c.join()
    d.join()

    devices = Device.objects.all()

    tax = round(len(devices) / 2)

    dev_a = devices[:tax]
    dev_b = devices[tax:]

    alt_a, alt_b = dev_a, dev_b

    dev_c, dev_a = alt_a[:round(len(dev_a) / 2)], alt_a[round(len(dev_a) / 2):]
    dev_d, dev_b = alt_b[:round(len(dev_b) / 2)], alt_b[round(len(dev_b) / 2):]

    # a = threading.Thread(target=pop_events, args=(devices,))
    a = threading.Thread(target=pop_events, args=(dev_a,))
    b = threading.Thread(target=pop_events, args=(dev_b,))
    c = threading.Thread(target=pop_events, args=(dev_c,))
    d = threading.Thread(target=pop_events, args=(dev_d,))

    print('----------------------------')
    print('Starting pop_events threads!')
    print('----------------------------')

    a.start()
    b.start()
    c.start()
    d.start()

    a.join()
    b.join()
    c.join()
    d.join()

    print('Device count is: %d' % len(Device.objects.all()))
    print('MeasureableEvent count is: %d' %
          len(MeasureableEvent.objects.all()))
    print('MeasureableVerticalEvent count is: %d' %
          len(MeasureableVerticalEvent.objects.all()))
    print('Sample count is: %d' % len(Sample.objects.all()))
    print('VerticalSample count is: %d' % len(VerticalSample.objects.all()))
    print('AltVerticalSample count is: %d' % len(AltVerticalSample.objects.all()))
